package com.legacy.lava_monster;

import java.util.List;

import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.biome.MobSpawnSettings.SpawnerData;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.world.BiomeModifier;
import net.neoforged.neoforge.common.world.BiomeModifiers;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

public class LMBiomeModifiers
{
	public static final ResourceKey<BiomeModifier> NORMAL_MONSTERS = modifier("add_normal_lava_monsters");
	public static final ResourceKey<BiomeModifier> NETHER_MONSTERS = modifier("add_nether_lava_monsters");

	public static final ResourceKey<BiomeModifier> NORMAL_COSTS = modifier("add_normal_lava_monster_costs");
	public static final ResourceKey<BiomeModifier> NETHER_COSTS = modifier("add_nether_lava_monster_costs");

	private static ResourceKey<BiomeModifier> modifier(String name)
	{
		return ResourceKey.create(NeoForgeRegistries.Keys.BIOME_MODIFIERS, LavaMonsterMod.locate(name));
	}

	@SuppressWarnings("deprecation")
	public static void modifierBootstrap(BootstrapContext<BiomeModifier> bootstrap)
	{
		var normalSettings = new MobSpawnSettings.SpawnerData(LMEntityTypes.LAVA_MONSTER, 130, 1, 1);
		bootstrap.register(NORMAL_MONSTERS, addSpawn(bootstrap, Tags.Biomes.IS_OVERWORLD, normalSettings));

		var netherSettings = new MobSpawnSettings.SpawnerData(LMEntityTypes.LAVA_MONSTER, 1, 1, 1);
		bootstrap.register(NETHER_MONSTERS, addSpawn(bootstrap, Tags.Biomes.IS_NETHER, netherSettings));

		var normalCosts = new MobSpawnSettings.MobSpawnCost(0.1F, 8F);
		bootstrap.register(NORMAL_COSTS, addSpawnCosts(bootstrap, Tags.Biomes.IS_OVERWORLD, HolderSet.direct(LMEntityTypes.LAVA_MONSTER.builtInRegistryHolder()), normalCosts));

		var netherCosts = new MobSpawnSettings.MobSpawnCost(0.1F, 10F);
		bootstrap.register(NETHER_COSTS, addSpawnCosts(bootstrap, Tags.Biomes.IS_NETHER, HolderSet.direct(LMEntityTypes.LAVA_MONSTER.builtInRegistryHolder()), netherCosts));
	}

	private static BiomeModifier addSpawn(BootstrapContext<?> bootstrap, TagKey<Biome> spawnTag, SpawnerData... data)
	{
		return new BiomeModifiers.AddSpawnsBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), List.of(data));
	}

	private static BiomeModifier addSpawnCosts(BootstrapContext<?> bootstrap, TagKey<Biome> spawnTag, HolderSet<EntityType<?>> entityTypes, MobSpawnSettings.MobSpawnCost spawnCost)
	{
		return new BiomeModifiers.AddSpawnCostsBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), entityTypes, spawnCost);
	}
}
