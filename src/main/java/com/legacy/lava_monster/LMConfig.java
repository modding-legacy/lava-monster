package com.legacy.lava_monster;

import org.apache.commons.lang3.tuple.Pair;

import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.config.ModConfigEvent;
import net.neoforged.neoforge.common.ModConfigSpec;

@EventBusSubscriber(modid = LavaMonsterMod.MODID, bus = EventBusSubscriber.Bus.MOD)
public class LMConfig
{
	public static final ModConfigSpec CLIENT_SPEC;
	public static final ModConfigSpec SERVER_SPEC;
	public static final ClientConfig CLIENT;
	public static final ServerConfig SERVER;

	public static float monsterHealth;
	public static double monsterArmor;
	public static double monsterRange;
	public static int attackCooldown;
	public static int attackShots;
	public static int attackSpacing;
	public static int attackWindup;

	public static String dimensionWhitelist;

	static
	{
		{
			final Pair<ClientConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = pair.getLeft();
			CLIENT_SPEC = pair.getRight();
		}
		{
			final Pair<ServerConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(ServerConfig::new);
			SERVER = pair.getLeft();
			SERVER_SPEC = pair.getRight();
		}
	}

	@SubscribeEvent
	public static void onLoadConfig(final ModConfigEvent event)
	{
		ModConfig config = event.getConfig();

		if (config.getSpec() == CLIENT_SPEC)
		{
			ConfigBakery.bakeClient(config);
		}
		else if (config.getSpec() == SERVER_SPEC)
		{
			ConfigBakery.bakeServer(config);
		}
	}

	private static class ClientConfig
	{
		public ClientConfig(ModConfigSpec.Builder builder)
		{
			builder.comment("Client side changes.").push("client");
			builder.pop();
		}
	}

	private static class ServerConfig
	{
		public final ModConfigSpec.ConfigValue<Double> monsterHealth;
		public final ModConfigSpec.ConfigValue<Double> monsterArmor;
		public final ModConfigSpec.ConfigValue<Double> monsterRange;

		public final ModConfigSpec.ConfigValue<Integer> attackCooldown;
		public final ModConfigSpec.ConfigValue<Integer> attackShots;
		public final ModConfigSpec.ConfigValue<Integer> attackSpacing;
		public final ModConfigSpec.ConfigValue<Integer> attackWindup;

		public final ModConfigSpec.ConfigValue<String> dimensionWhitelist;

		public ServerConfig(ModConfigSpec.Builder builder)
		{
			builder.comment("Common changes").push("common");
			monsterHealth = builder.translation("monsterHealth").comment("Lava monsters' maximum health.").define("monsterHealth", 16.0D);
			monsterArmor = builder.translation("monsterArmor").comment("The amount of armor lava monsters have.").define("monsterArmor", 0.0D);
			monsterRange = builder.translation("monsterRange").comment("The distance a lava monster will begin attacking. (maximum of 35)").define("monsterRange", 17.0D);
			attackCooldown = builder.translation("attackCooldown").comment("Ticks a monster must wait after attacking before it can start winding up again.").define("attackCooldown", 80);
			attackShots = builder.translation("attackShots").comment("Number of fireballs shot with each attack.").define("attackShots", 3);
			attackSpacing = builder.translation("attackSpacing").comment("Ticks between each fireball shot in an attack.").define("attackSpacing", 6);
			attackWindup = builder.translation("attackWindup").comment("Ticks it takes before a monster can start an attack.").define("attackWindup", 60);

			builder.comment("Spawning").push("spawning");

			dimensionWhitelist = builder.translation("dimensionWhitelist").comment("Allows you to choose which dimensions the Lava Monster spawns in. Add dimensions by their registry name/id, and separate them with commas, no spaces.").define("dimensionWhitelist", "minecraft:overworld,minecraft:the_nether,good_nights_sleep:nightmare");

			builder.pop();
			builder.pop();
		}
	}

	@SuppressWarnings("unused")
	private static class ConfigBakery
	{
		private static ModConfig clientConfig;
		private static ModConfig serverConfig;

		public static void bakeClient(ModConfig config)
		{
			clientConfig = config;
		}

		public static void bakeServer(ModConfig config)
		{
			serverConfig = config;
			monsterHealth = SERVER.monsterHealth.get().floatValue();
			monsterArmor = SERVER.monsterArmor.get();
			monsterRange = SERVER.monsterRange.get();
			attackCooldown = SERVER.attackCooldown.get();
			attackShots = SERVER.attackShots.get();
			attackSpacing = SERVER.attackSpacing.get();
			attackWindup = SERVER.attackWindup.get();

			dimensionWhitelist = SERVER.dimensionWhitelist.get();
		}
	}
}
