package com.legacy.lava_monster;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import net.minecraft.DetectedVersion;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraft.util.InclusiveRange;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.data.DatapackBuiltinEntriesProvider;
import net.neoforged.neoforge.data.event.GatherDataEvent;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

@EventBusSubscriber(modid = LavaMonsterMod.MODID, bus = Bus.MOD)
public class LMDataGen
{
	private static final RegistrySetBuilder BUILDER = new RegistrySetBuilder().add(NeoForgeRegistries.Keys.BIOME_MODIFIERS, LMBiomeModifiers::modifierBootstrap);

	@SubscribeEvent
	public static void gatherData(GatherDataEvent event) throws InterruptedException, ExecutionException
	{
		DataGenerator gen = event.getGenerator();
		PackOutput output = gen.getPackOutput();
		boolean server = event.includeServer();

		gen.addProvider(server, new DatapackBuiltinEntriesProvider(output, event.getLookupProvider(), BUILDER, Set.of(LavaMonsterMod.MODID)));

		gen.addProvider(server, packMcmeta(output, "Lava Monsters resources"));
	}

	private static final DataProvider packMcmeta(PackOutput output, String description)
	{
		int serverVersion = DetectedVersion.BUILT_IN.getPackVersion(PackType.SERVER_DATA);
		return NestedDataProvider.of(new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), serverVersion, Optional.of(new InclusiveRange<>(0, Integer.MAX_VALUE)))), description);
	}

	private record NestedDataProvider<D extends DataProvider>(D provider, String namePrefix) implements DataProvider
	{

		public static <D extends DataProvider> NestedDataProvider<D> of(D provider, String namePrefix)
		{
			return new NestedDataProvider<>(provider, namePrefix);
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cachedOutput)
		{
			return this.provider.run(cachedOutput);
		}

		@Override
		public String getName()
		{
			return this.namePrefix + "/" + this.provider.getName();
		}
	}
}