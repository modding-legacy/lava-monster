package com.legacy.lava_monster;

import com.legacy.lava_monster.entity.LavaMonsterEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.neoforged.neoforge.event.entity.EntityAttributeCreationEvent;

public class LMEntityTypes
{
	public static final EntityType<LavaMonsterEntity> LAVA_MONSTER = buildEntity("lava_monster", EntityType.Builder.of(LavaMonsterEntity::new, MobCategory.MONSTER).fireImmune().sized(0.8F, 2.2F).eyeHeight(1.5F));

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(ResourceKey.create(Registries.ENTITY_TYPE, LavaMonsterMod.locate(key)));
	}

	public static void onAttributesRegistered(EntityAttributeCreationEvent event)
	{
		event.put(LAVA_MONSTER, LavaMonsterEntity.registerAttributeMap().build());
	}
}