package com.legacy.lava_monster;

import com.legacy.lava_monster.entity.LavaMonsterEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.entity.SpawnPlacementTypes;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.level.levelgen.Heightmap;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;
import net.neoforged.neoforge.event.entity.RegisterSpawnPlacementsEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

public class LMRegistryHandler
{
	public static final Lazy<Item> LAVA_MONSTER_SPAWN_EGG = Lazy.of(() -> new SpawnEggItem(LMEntityTypes.LAVA_MONSTER, 0xda7522, 0xdab022, new Item.Properties().setId(ResourceKey.create(Registries.ITEM, LavaMonsterMod.locate("lava_monster_spawn_egg")))));

	@SubscribeEvent
	public static void onRegisterItems(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(Registries.ENTITY_TYPE))
		{
			event.register(Registries.ENTITY_TYPE, LavaMonsterMod.locate("lava_monster"), () -> LMEntityTypes.LAVA_MONSTER);
		}
		else if (event.getRegistryKey().equals(Registries.ITEM))
			event.register(Registries.ITEM, LavaMonsterMod.locate("lava_monster_spawn_egg"), LAVA_MONSTER_SPAWN_EGG);
	}

	public static void registerSpawnPlacements(RegisterSpawnPlacementsEvent event)
	{
		event.register(LMEntityTypes.LAVA_MONSTER, SpawnPlacementTypes.IN_LAVA, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, LavaMonsterEntity::getVanillaSpawnConditions, RegisterSpawnPlacementsEvent.Operation.REPLACE);
	}

	public static void registerCreativeTabs(BuildCreativeModeTabContentsEvent event)
	{
		if (event.getTabKey() == CreativeModeTabs.SPAWN_EGGS)
			event.accept(LAVA_MONSTER_SPAWN_EGG.get());
	}
}