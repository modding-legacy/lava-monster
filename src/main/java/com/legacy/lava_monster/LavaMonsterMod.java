package com.legacy.lava_monster;

import com.legacy.lava_monster.client.LMEntityRendering;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.EventPriority;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.loading.FMLEnvironment;

@Mod(LavaMonsterMod.MODID)
public class LavaMonsterMod
{
	public static final String NAME = "Lava Monster";
	public static final String MODID = "lava_monster";

	public static ResourceLocation locate(String path)
	{
		return ResourceLocation.fromNamespaceAndPath(MODID, path);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public LavaMonsterMod(IEventBus modBus, ModContainer modContainer)
	{
		modContainer.registerConfig(ModConfig.Type.COMMON, LMConfig.SERVER_SPEC);

		if (FMLEnvironment.dist == Dist.CLIENT)
			LMEntityRendering.init(modBus);

		modBus.addListener(LMEntityTypes::onAttributesRegistered);
		modBus.addListener(LMRegistryHandler::registerCreativeTabs);
		modBus.addListener(EventPriority.LOWEST, LMRegistryHandler::registerSpawnPlacements);
		modBus.register(LMRegistryHandler.class);
	}
}
