package com.legacy.lava_monster.client;

import com.legacy.lava_monster.LMEntityTypes;
import com.legacy.lava_monster.LavaMonsterMod;

import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;

public class LMEntityRendering
{
	public static final ModelLayerLocation LAVA_MONSTER_LAYER = new ModelLayerLocation(LavaMonsterMod.locate("lava_monster"), "main");

	public static void init(IEventBus modBus)
	{
		modBus.addListener(LMEntityRendering::initLayers);
		modBus.addListener(LMEntityRendering::initRenders);
	}

	public static void initLayers(EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		event.registerLayerDefinition(LAVA_MONSTER_LAYER, () -> LavaMonsterModel.createBodyLayer(CubeDeformation.NONE));
	}

	public static void initRenders(EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(LMEntityTypes.LAVA_MONSTER, LavaMonsterRenderer::new);
	}
}