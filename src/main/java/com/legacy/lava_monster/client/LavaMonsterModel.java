package com.legacy.lava_monster.client;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class LavaMonsterModel<S extends LavaMonsterRenderState> extends EntityModel<S>
{
	private final ModelPart head;
	private final ModelPart jaw;
	protected final ModelPart body;
	private final ModelPart rightHindLeg;
	private final ModelPart leftHindLeg;
	private final ModelPart rightFrontLeg;
	private final ModelPart leftFrontLeg;

	public LavaMonsterModel(ModelPart model)
	{
		super(model);
		this.head = model.getChild("head");
		this.jaw = model.getChild("jaw");

		this.body = model.getChild("body");

		this.rightFrontLeg = model.getChild("right_front_leg");
		this.leftFrontLeg = model.getChild("left_front_leg");
		this.rightHindLeg = model.getChild("right_hind_leg");
		this.leftHindLeg = model.getChild("left_hind_leg");
	}

	public static LayerDefinition createBodyLayer(CubeDeformation size)
	{
		MeshDefinition mesh = new MeshDefinition();
		PartDefinition root = mesh.getRoot();

		root.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-3.5F, -6F, -8F, 7, 6, 10, size), PartPose.offset(0.0F, 2.0F, -3.0F));
		root.addOrReplaceChild("jaw", CubeListBuilder.create().texOffs(34, 0).addBox(-3F, -1F, -7F, 6, 2, 7, size), PartPose.offset(0.0F, 2.0F, -3.0F));

		root.addOrReplaceChild("body", CubeListBuilder.create().texOffs(32, 9).addBox(-4.5F, -10F, -7F, 9, 16, 7, size), PartPose.offset(0.0F, 11.0F, 3.0F));

		root.addOrReplaceChild("right_front_leg", CubeListBuilder.create().texOffs(0, 18).addBox(-2F, -2F, -2F, 4, 10, 4, size), PartPose.offset(-5.0F, 16.0F, -5.0F));
		root.addOrReplaceChild("left_front_leg", CubeListBuilder.create().texOffs(0, 18).addBox(-2F, -2F, -2F, 4, 10, 4, size).mirror(), PartPose.offset(5.0F, 16.0F, -5.0F));
		root.addOrReplaceChild("right_hind_leg", CubeListBuilder.create().texOffs(0, 18).addBox(-2F, -2F, -3F, 4, 10, 4, size), PartPose.offset(-5.0F, 16.0F, 5.0F));
		root.addOrReplaceChild("left_hind_leg", CubeListBuilder.create().texOffs(0, 18).addBox(-2F, -2F, -3F, 4, 10, 4, size).mirror(), PartPose.offset(5.0F, 16.0F, 5.0F));

		return LayerDefinition.create(mesh, 64, 32);
	}

	@Override
	public void setupAnim(S state)
	{
		this.head.xRot = state.xRot / 57.29578F;
		this.head.yRot = state.yRot / 57.29578F;
		this.jaw.xRot = this.head.xRot + 0.392699F;
		this.jaw.yRot = this.head.yRot;
		this.rightHindLeg.xRot = Mth.cos(state.walkAnimationPos * 0.6662F + (float) Math.PI) * 1.4F * state.walkAnimationSpeed;
		this.leftHindLeg.xRot = Mth.cos(state.walkAnimationPos * 0.6662F) * 1.4F * state.walkAnimationSpeed;
		this.rightFrontLeg.xRot = this.leftHindLeg.xRot;
		this.leftFrontLeg.xRot = this.rightHindLeg.xRot;
		
		this.jaw.xRot += Mth.sin(state.ageInTicks * 0.07F) * 0.15F;
	}
}