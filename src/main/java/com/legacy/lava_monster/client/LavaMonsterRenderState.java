package com.legacy.lava_monster.client;

import net.minecraft.client.renderer.entity.state.LivingEntityRenderState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class LavaMonsterRenderState extends LivingEntityRenderState
{
	public int textureIndex;

}
