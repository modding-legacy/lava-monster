package com.legacy.lava_monster.client;

import com.legacy.lava_monster.LavaMonsterMod;
import com.legacy.lava_monster.entity.LavaMonsterEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

public class LavaMonsterRenderer extends MobRenderer<LavaMonsterEntity, LavaMonsterRenderState, LavaMonsterModel<LavaMonsterRenderState>>
{
	private static final ResourceLocation BASE_TEXTURE = LavaMonsterMod.locate("textures/entity/lava_monster_0.png");
	public static final ResourceLocation[] LAVA_MONSTER_TEXTURES = new ResourceLocation[20];
	static
	{
		String path = LavaMonsterMod.find("textures/entity/lava_monster_");

		for (int i = 0; i < LavaMonsterRenderer.LAVA_MONSTER_TEXTURES.length; i++)
			LavaMonsterRenderer.LAVA_MONSTER_TEXTURES[i] = ResourceLocation.parse(path + Integer.toString(i) + ".png");
	}

	public LavaMonsterRenderer(EntityRendererProvider.Context context)
	{
		super(context, new LavaMonsterModel<>(context.bakeLayer(LMEntityRendering.LAVA_MONSTER_LAYER)), 0.5F);
	}

	@Override
	protected void scale(LavaMonsterRenderState state, PoseStack pose)
	{
		pose.scale(1.25F, 1.25F, 1.25F);
	}

	@Override
	public ResourceLocation getTextureLocation(LavaMonsterRenderState state)
	{
		return Minecraft.useFancyGraphics() ? LavaMonsterRenderer.LAVA_MONSTER_TEXTURES[state.textureIndex] : BASE_TEXTURE;
	}

	@Override
	public LavaMonsterRenderState createRenderState()
	{
		return new LavaMonsterRenderState();
	}

	@Override
	public void extractRenderState(LavaMonsterEntity entity, LavaMonsterRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);
		state.textureIndex = entity.getTextureIndex();
	}
}