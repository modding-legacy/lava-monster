package com.legacy.lava_monster.entity;

import com.legacy.lava_monster.LMConfig;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.util.Mth;

public class LavaMonsterAttackGoal extends Goal
{
	/// The entity (host) using this AI.
	protected LavaMonsterEntity lavaMonster;
	/// The host's target.
	public LivingEntity target;
	/// The host's moveSpeed.
	public double moveSpeed;
	/// Ticks until the host stops after seeing its target.
	public byte pathDelay = 0;
	/// The attack phase the host is currently in.
	public byte attackPhase = 0;

	public LavaMonsterAttackGoal(LavaMonsterEntity entity, double speed)
	{
		this.lavaMonster = entity;
		this.moveSpeed = speed;
	}

	@Override
	public boolean canUse()
	{
		LivingEntity e = lavaMonster.getTarget();

		if (e == null)
			return false;

		this.target = e;
		return true;
	}

	@Override
	public boolean canContinueToUse()
	{
		return this.canUse() || !lavaMonster.getNavigation().isDone();
	}

	@Override
	public void stop()
	{
		this.lavaMonster.setTarget(null);
		this.target = null;
	}

	@Override
	public void tick()
	{
		if (this.target == null)
			return;

		float f = (float) (lavaMonster.getX() - target.getX());
		float f1 = (float) lavaMonster.getY() - (float) lavaMonster.getY();
		float f2 = (float) (lavaMonster.getZ() - target.getZ());

		double distance = Mth.sqrt(f * f + f1 * f1 + f2 * f2);

		boolean canSee = lavaMonster.getSensing().hasLineOfSight(target);
		double range = Math.min(35.0D, 17);

		if (canSee)
			pathDelay = (byte) Math.min(20, pathDelay + 1);
		else
			pathDelay = 0;
		if (distance <= range && pathDelay >= 20)
			lavaMonster.getNavigation().stop();
		else
			lavaMonster.getNavigation().moveTo(target, moveSpeed);
		lavaMonster.getLookControl().setLookAt(target, 20.0F, 20.0F);
		if (lavaMonster.attackDelay > 0)
			return;

		if (distance > range || !canSee)
		{
			lavaMonster.attackDelay = 10;
			return;
		}

		if (++attackPhase == 1)
		{
			lavaMonster.attackDelay = LMConfig.attackWindup;
			return;
		}

		lavaMonster.performRangedAttack(target, 0);

		if (attackPhase <= LMConfig.attackShots)
		{
			lavaMonster.attackDelay = LMConfig.attackSpacing;
		}
		else
		{
			lavaMonster.attackDelay = LMConfig.attackCooldown;
			attackPhase = 0;
		}
	}
}