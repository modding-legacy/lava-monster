package com.legacy.lava_monster.entity;

import com.legacy.lava_monster.LMConfig;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.animal.SnowGolem;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.monster.RangedAttackMob;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.SmallFireball;
import net.minecraft.world.entity.projectile.Snowball;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.BaseFireBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.pathfinder.PathType;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.Tags.Biomes;

public class LavaMonsterEntity extends Monster implements RangedAttackMob
{
	/// Ticks until the next attack phase change.
	public int attackDelay = 0;
	/// True if the texture index is increasing.
	private boolean textureInc = true;
	/// Counter to next texture update.
	private byte textureTicks = 0;
	/// The current texture index.
	private byte textureIndex = 0;

	public LavaMonsterEntity(EntityType<? extends LavaMonsterEntity> type, Level world)
	{
		super(type, world);

		this.setPathfindingMalus(PathType.WATER, -1.0F);
		this.setPathfindingMalus(PathType.LAVA, 0.0F);
		this.setPathfindingMalus(PathType.DANGER_FIRE, 0.0F);
		this.setPathfindingMalus(PathType.DAMAGE_FIRE, 0.0F);
		this.setPathfindingMalus(PathType.OPEN, 0.0F);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(3, new AvoidEntityGoal<>(this, SnowGolem.class, 6.0F, 1.0D, 1.2D));
		this.goalSelector.addGoal(3, new LavaMonsterAttackGoal(this, 1.0D));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 1.0D));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, Player.class, 8.0F));
		this.goalSelector.addGoal(6, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)).setAlertOthers());
		this.targetSelector.addGoal(1, new HurtByTargetGoal(this));
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Player.class, true));
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, EntitySpawnReason reason, SpawnGroupData spawnDataIn)
	{
		this.getAttribute(Attributes.MAX_HEALTH).setBaseValue(LMConfig.monsterHealth);
		this.getAttribute(Attributes.ARMOR).setBaseValue(LMConfig.monsterArmor);
		this.setHealth(LMConfig.monsterHealth);

		/*System.out.println("spawned " + this.blockPosition());*/
		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn);
	}

	public static boolean getVanillaSpawnConditions(EntityType<LavaMonsterEntity> type, LevelAccessor world, EntitySpawnReason reason, BlockPos pos, RandomSource rand)
	{
		BlockPos.MutableBlockPos blockpos$mutable = pos.mutable();

		do
		{
			blockpos$mutable.move(Direction.UP);
		}
		while (world.getFluidState(blockpos$mutable).is(FluidTags.LAVA));

		return world.getBlockState(blockpos$mutable).isAir();
	}

	@Override
	public boolean checkSpawnObstruction(LevelReader world)
	{
		return world.isUnobstructed(this);
	}

	@Override
	public float getWalkTargetValue(BlockPos pos, LevelReader worldIn)
	{
		return 5.0F;
	}

	public static AttributeSupplier.Builder registerAttributeMap()
	{
		return Monster.createMonsterAttributes().add(Attributes.MAX_HEALTH, 16.0D).add(Attributes.FOLLOW_RANGE, 35.0D).add(Attributes.MOVEMENT_SPEED, 0.24F).add(Attributes.ATTACK_DAMAGE, 3.0D).add(Attributes.ARMOR, 0.0D);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return null;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return SoundEvents.BUCKET_EMPTY_LAVA;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return SoundEvents.GHAST_DEATH;
	}

	@Override
	public void aiStep()
	{
		super.aiStep();

		if (this.level().isClientSide())
		{
			this.updateTexture();
		}

		if (this.random.nextInt(100) == 0)
		{
			this.level().addParticle(ParticleTypes.LAVA, this.getX() + (this.random.nextDouble() - 0.5) * this.getBbWidth(), this.getY() + this.random.nextDouble() * this.getBbHeight() + this.getBbHeight() / 2.0, this.getZ() + (this.random.nextDouble() - 0.5) * this.getBbWidth(), 0.0, 0.0, 0.0);
			this.playSound(SoundEvents.LAVA_POP, 0.2F + this.random.nextFloat() * 0.2F, 0.9F + this.random.nextFloat() * 0.15F);
		}

		if (this.random.nextInt(200) == 0)
			this.playSound(SoundEvents.LAVA_AMBIENT, 0.2F + this.random.nextFloat() * 0.2F, 0.9F + this.random.nextFloat() * 0.15F);
	}

	@Override
	protected void customServerAiStep(ServerLevel level)
	{
		super.customServerAiStep(level);

		if (this.isInLava())
			this.setDeltaMovement(this.getDeltaMovement().multiply(1.9D, 1.0D, 1.9D));

		if (this.getTarget() != null)
			this.getLookControl().setLookAt(this.getTarget(), 20.0F, 20.0F);

		this.attackDelay = Math.max(0, this.attackDelay - 1);

		if (this.isInWaterOrRain())
			this.hurtServer(level, this.damageSources().drown(), 1);

		BlockPos pos = new BlockPos(Mth.floor(this.getX()), Mth.floor(this.getY()), Mth.floor(this.getZ()));
		BlockState fire = BaseFireBlock.getState(level, pos);

		if (level.isEmptyBlock(pos) && level.getGameRules().getBoolean(GameRules.RULE_MOBGRIEFING) && level.getGameRules().getBoolean(GameRules.RULE_DOFIRETICK) && fire.canSurvive(level, pos))
			level.setBlock(pos, fire, 2);
	}
	
	@Override
	public int getMaxSpawnClusterSize()
	{
		return 1;
	}

	@Override
	public boolean isMaxGroupSizeReached(int size)
	{
		return size >= 1;
	}

	@Override
	public boolean hurtServer(ServerLevel level, DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(level, source))
			return false;
		else if (source.getDirectEntity() instanceof Snowball)
			return super.hurtServer(level, source, Math.max(3.0F, amount));
		else
			return super.hurtServer(level, source, amount);
	}

	@Override
	public void performRangedAttack(LivingEntity target, float distanceFactor)
	{
		SmallFireball fireball = new SmallFireball(this.level(), this, new Vec3(target.getX() - this.getX(), target.getBoundingBox().minY + target.getBbHeight() / 2.0F - this.getY() - this.getBbHeight() / 2.0F, target.getZ() - this.getZ()));
		fireball.setPos(fireball.getX(), this.getY() + this.getBbHeight() - 0.5, fireball.getZ());
		this.playSound(SoundEvents.GHAST_SHOOT, 1.0F, 1.0F / (this.random.nextFloat() * 0.4F + 0.8F));
		this.level().addFreshEntity(fireball);
	}

	@Override
	public boolean checkSpawnRules(LevelAccessor worldIn, EntitySpawnReason spawnReasonIn)
	{
		boolean extraChance = /*LavaMonsterConfig.shouldUseVanillaSpawning*/ worldIn.dimensionType().ultraWarm() ? worldIn.getRandom().nextFloat() < 0.2F : true;

		/*if (extraChance)
			System.out.println("checkSpawnRules " + extraChance);*/

		return extraChance && this.isWhitelistedToDimension() && !worldIn.getBiome(this.blockPosition()).is(Tags.Biomes.IS_MUSHROOM);
	}

	/*public boolean classicSpawnRules() // areCollisionShapesEmpty areCollisionBoxesEmpty
	{
		return this.random.nextDouble() < LavaMonsterConfig.spawnChance && this.level().noCollision(this, this.getBoundingBox()) && this.isWhitelistedToDimension();
	}*/

	public boolean isWhitelistedToDimension()
	{
		String[] biomeArray = LMConfig.dimensionWhitelist.split(",");

		ResourceLocation dimLocation = this.level().dimension().location();
		boolean isWhitelisted = false;

		if (!isWhitelisted)
		{
			for (String name : biomeArray)
			{
				ResourceLocation regName = ResourceLocation.parse(name);

				if (dimLocation.toString().matches(regName.toString()))
					isWhitelisted = true;
			}

		}

		return isWhitelisted;
	}

	public void updateTexture()
	{
		if (++this.textureTicks < 2)
			return;

		this.textureTicks = 0;
		this.textureIndex += this.textureInc ? 1 : -1;

		if (this.textureIndex < 0)
		{
			this.textureIndex = 1;
			this.textureInc = true;
		}
		else if (this.textureIndex > 19)
		{
			this.textureIndex = 18;
			this.textureInc = false;
		}
	}

	public int getTextureIndex()
	{
		return this.textureIndex;
	}

	@Override
	public ItemEntity spawnAtLocation(ServerLevel level, ItemStack stack, float offsetY)
	{
		if (stack.isEmpty())
		{
			return null;
		}
		else
		{
			ItemEntity itementity = new ItemEntity(this.level(), this.getX(), this.getY() + (double) offsetY, this.getZ(), stack);
			itementity.setDefaultPickUpDelay();
			itementity.setInvulnerable(true);
			if (captureDrops() != null)
				captureDrops().add(itementity);
			else
				this.level().addFreshEntity(itementity);
			return itementity;
		}
	}
}